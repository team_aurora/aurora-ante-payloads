const _ = require('lodash');
const async = require('async');
const moment = require('moment');
const btoa = require('btoa');

const startFrame = 'FEFEFEFE';
let firstHead;
let seconHead;
let tail;
let elementsDefaultPass;
var exports = module.exports;
var hexArray = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];

exports.config = (config) => {
  firstHead = config.trama.firstHead;
  seconHead = config.trama.seconHead;
  tail = config.trama.tail;
  elementsDefaultPass = config.elementsDefaultPass;
};


// ------------------------------------ CREATE WRITE FRAME FOR SERVICES -------------------------

exports.createWriteEnergyFrame = function createWriteEnergyFrame(element, identificationCode, commandValue) {
  console.log("writer energy frame", element, identificationCode, commandValue);

  let extraValue = '04';
  let meterId = element.elementId;

  return new Promise((resolve, reject) => {
    meterId = this.completeMeterId(meterId, (element.schemaData.defaultChar || '0'));

    const elementDefaultPass = this.elementDefaultPassFormatted();
    let dataLength = identificationCode + elementDefaultPass + commandValue;

    identificationCode = this.formatIdentificationCode(identificationCode); //get formated load code
    commandValue = this.convertToHexa(commandValue);
    commandValue = this.reversePair(commandValue);
    dataLength = Math.round(dataLength.length / 2);
    extraValue += (dataLength + 0x10000).toString(16).substr(-2).toUpperCase();

    const checksum = this.checksum256(firstHead + meterId + seconHead + extraValue + identificationCode + elementDefaultPass + commandValue);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + firstHead + meterId + seconHead + extraValue + identificationCode + elementDefaultPass + commandValue + checksum + tail;
    resolve(frame);
  });
};

exports.createWriteWaterFrame = function createWriteWaterFrame(element, identificationCode, identificationcodes, commandValue) {

  let meterId = element.elementId;

  return new Promise((resolve, reject) => {
    meterId = this.completeMeterId(meterId, (element.schemaData.defaultChar || '0'));
    const identificationCodeData = _.find(identificationcodes, function (ic) { return ic.code == identificationCode; });
    const initFrame = firstHead + meterId + seconHead + identificationCodeData.extra.controlCode + identificationCodeData.extra.fieldLenght + identificationCode + identificationCodeData.extra.userDefined + commandValue;
    const checksum = this.checksum256(initFrame);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + initFrame + checksum + tail;
    resolve(frame);
  });

};

exports.createWriteGasFrame = function createWriteGasFrame(element, identificationCode, identificationcodes) {

  let meterId = element.elementId;

  return new Promise((resolve, reject) => {
    meterId = this.reversePair(meterId) + 'AAAA'
    const identificationCodeData = _.find(identificationcodes, function (ic) { return ic.code == identificationCode; });
    const initFrame = firstHead + meterId + seconHead + identificationCodeData.extra.controlCode + identificationCodeData.extra.fieldLenght + identificationCode + identificationCodeData.extra.userDefined;
    const checksum = this.checksum256(initFrame);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = initFrame + checksum + tail;
    resolve(frame);
  });

};

// ------------------------------------ CREATE READ FRAME FOR SERVICES -------------------------

exports.createEnergyReadFrame = function createEnergyReadFrame(element, identificationCode) {

  let meterId = element.elementId;
  const extraValue = '0102';
  const _this = this;

  return new Promise((resolve, reject) => {
    meterId = _this.completeMeterId(meterId, (element.schemaData.defaultChar || '0'));
    identificationCode = _this.formatIdentificationCode(identificationCode); //get formated load code
    const checksum = _this.checksum256(firstHead + meterId + seconHead + extraValue + identificationCode);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + firstHead + meterId + seconHead + extraValue + identificationCode + checksum + tail;
    resolve(frame);
  });
};


exports.createEnergyReadRegisterFrame = function createEnergyReadRegisterFrame(element, identificationCode, payload) {

  let meterId = element.elementId;
  const extraValue = '0104';
  const _this = this;
  return new Promise((resolve, reject) => {
    meterId = _this.completeMeterId(meterId, (element.schemaData.defaultChar || '0'));
    identificationCode = _this.formatIdentificationCode(identificationCode); //get formated load code
    const registerCode = _this.formatIdentificationCode(payload);
    const checksum = _this.checksum256(firstHead + meterId + seconHead + extraValue + identificationCode + registerCode);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + firstHead + meterId + seconHead + extraValue + identificationCode + registerCode + checksum + tail;
    resolve(frame);
  });
};


exports.createWaterReadFrame = function createWaterReadFrame(element, identificationCode, identificationcodes) {
  let meterId = element.elementId;
  return new Promise((resolve, reject) => {
    const identificationCodeData = _.find(identificationcodes, function (ic) { return ic.code == identificationCode; });
    meterId = this.completeMeterId(meterId, (element.schemaData.defaultChar || '0'));
    const initFrame = firstHead + meterId + seconHead + identificationCodeData.extra.controlCode + identificationCodeData.extra.fieldLenght + identificationCode + identificationCodeData.extra.userDefined;
    const checksum = this.checksum256(initFrame);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + initFrame + checksum + tail;
    resolve(frame);
  });
};



exports.createGasReadFrame = function createGasReadFrame(element, identificationCode, identificationcodes) {
  let meterId = element.elementId;

  return new Promise((resolve, reject) => {
    meterId = this.completeMeterId(meterId, (element.schemaData.defaultChar || '0'));
    const identificationCodeData = _.find(identificationcodes, function (ic) { return ic.code == identificationCode; });
    const initFrame = firstHead + meterId + seconHead + identificationCodeData.extra.controlCode + identificationCodeData.extra.fieldLenght + identificationCode + identificationCodeData.extra.userDefined;
    const checksum = this.checksum256(initFrame);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + initFrame + checksum + tail;
    resolve(frame);
  });
};

// ------------------------------------ CREATE READ PROFILE FRAME FOR ENERGY -------------------------

exports.createProfileFrame = function createProfileFrame(element, identificationCode, loadLog) {
  let meterId = element.elementId;
  let extraValue = '01';
  const _this = this;

  return new Promise((resolve, reject) => {
    meterId = _this.completeMeterId(meterId, '0');

    let dataLength = identificationCode;
    identificationCode = _this.formatIdentificationCode(identificationCode); //get formated load code
    loadLog = _this.padWithChar(loadLog, 4, '0');
    loadLog = _this.reversePair(loadLog);
    loadLog = _this.convertToHexa(loadLog);

    dataLength = identificationCode + loadLog;
    dataLength = Math.round(dataLength.length / 2);
    extraValue += (dataLength < 10) ? '0' + dataLength : dataLength;

    const checksum = _this.checksum256(firstHead + meterId + seconHead + extraValue + identificationCode + loadLog);
    if (checksum == 'Non-hex character entered') {
      reject({ error: 'checksum256 invalid, Non-hex character entered', elementId: element.elementId, idCode: identificationCode });
    }
    const frame = startFrame + firstHead + meterId + seconHead + extraValue + identificationCode + loadLog + checksum + tail;
    resolve(frame);
  });
};

// ------------------------------------- DECODER ENERGY ----------------------------------------------

exports.energyDecoder = function energyDecoder(frame) {

  const tramaLenght = frame.length;
  const _this = this;

  return new Promise(resolve => {
    const result = {
      firstHead: _this.getFormatChunk(frame, 0, 2),
      meterNumber: _this.getFormatChunk(frame, 2, 14, 'meterNumber'),
      secondHead: _this.getFormatChunk(frame, 14, 16),
      controlCode: _this.getFormatChunk(frame, 16, 18),
      dataLength: _this.getFormatChunk(frame, 18, 20),
      identificationCode: _this.getFormatChunk(frame, 20, 24, 'identificationCode'),
      data: _this.getFormatChunk(frame, 24, (tramaLenght - 4), 'data'),
      calibrationCode: _this.getFormatChunk(frame, (tramaLenght - 4), (tramaLenght - 2)),
      tail: _this.getFormatChunk(frame, (tramaLenght - 2), tramaLenght)
    };

    setTimeout(function () {
      resolve(result);
    }, 3000);
  });
};

exports.waterDecoder = function waterDecoder(frame) {
  const tramaLenght = frame.length;
  const _this = this;

  return new Promise(resolve => {
    const identificationCode = _this.getFormatChunk(frame, 20, 22)
    if(identificationCode === '01'){
      const result = {
        firstHead: _this.getFormatChunk(frame, 0, 2),
        meterNumber: _this.getFormatChunk(frame, 2, 14, 'meterNumber'),
        secondHead: _this.getFormatChunk(frame, 14, 16),
        controlCode: _this.getFormatChunk(frame, 16, 18),
        dataLength: _this.getFormatChunk(frame, 18, 20),
        identificationCode: _this.getFormatChunk(frame, 20, 22),
        constant: _this.getFormatChunk(frame, 22, 24),
        data: _this.getFormatChunk(frame, 24, (tramaLenght - 12)),
        meterStatus: _this.getFormatChunk(frame, (tramaLenght - 12), (tramaLenght - 8), 'byte'),
        batteryVolume: _this.getFormatChunk(frame, (tramaLenght - 8), (tramaLenght - 4)),
        calibrationCode: _this.getFormatChunk(frame, (tramaLenght - 4), (tramaLenght - 2)),
        tail: _this.getFormatChunk(frame, (tramaLenght - 2), tramaLenght),
      };
      setTimeout(function () {
        resolve(result)
      }, 3000);
    }
    else {
      const identificationCode = _this.getFormatChunk(frame, 20, 22)

      const result = {
        firstHead: _this.getFormatChunk(frame, 0, 2),
        meterNumber: _this.getFormatChunk(frame, 2, 14, 'meterNumber'),
        secondHead: _this.getFormatChunk(frame, 14, 16),
        controlCode: _this.getFormatChunk(frame, 16, 18),
        dataLength: _this.getFormatChunk(frame, 18, 20),
        identificationCode,
        constant: _this.getFormatChunk(frame, 22, 24),
        data: identificationCode === '0A' ? _this.getFormatChunk(frame, 24, 28) : _this.getFormatChunk(frame, 24, (tramaLenght - 4)),
        tail: _this.getFormatChunk(frame, (tramaLenght - 2), tramaLenght)
      };
      // setTimeout(function () {
      resolve(result)
      // }, 3000);
    }
  });
};

exports.gasDecoder = function gasDecoder(frame) {
  const tramaLenght = frame.length;
  const _this = this;

  return new Promise(resolve => {
    const identificationCode = _this.getFormatChunk(frame, 20, 22)
    if (identificationCode === '01') {
      const result = {
        firstHead: _this.getFormatChunk(frame, 0, 2),
        meterNumber: _this.getFormatChunk(frame, 2, 14, 'meterNumber'),
        secondHead: _this.getFormatChunk(frame, 14, 16),
        controlCode: _this.getFormatChunk(frame, 16, 18),
        dataLength: _this.getFormatChunk(frame, 18, 20),
        identificationCode: _this.getFormatChunk(frame, 20, 22),
        constant: _this.getFormatChunk(frame, 22, 24),
        data: _this.getFormatChunk(frame, 24, (tramaLenght - 12)),
        meterStatus: _this.getFormatChunk(frame, (tramaLenght - 12), (tramaLenght - 8), 'byte'),
        batteryVolume: _this.getFormatChunk(frame, (tramaLenght - 8), (tramaLenght - 4)),
        calibrationCode: _this.getFormatChunk(frame, (tramaLenght - 4), (tramaLenght - 2)),
        tail: _this.getFormatChunk(frame, (tramaLenght - 2), tramaLenght),
      };
      // setTimeout(function () {
      resolve(result)
      // }, 3000);
    } else {
      const identificationCode = _this.getFormatChunk(frame, 20, 22)

      const result = {
        firstHead: _this.getFormatChunk(frame, 0, 2),
        meterNumber: _this.getFormatChunk(frame, 2, 14, 'meterNumber'),
        secondHead: _this.getFormatChunk(frame, 14, 16),
        controlCode: _this.getFormatChunk(frame, 16, 18),
        dataLength: _this.getFormatChunk(frame, 18, 20),
        identificationCode,
        constant: _this.getFormatChunk(frame, 22, 24),
        data: identificationCode === '0A' ? _this.getFormatChunk(frame, 24, 28) : _this.getFormatChunk(frame, 24, (tramaLenght - 4)),
        tail: _this.getFormatChunk(frame, (tramaLenght - 2), tramaLenght)
      };
      // setTimeout(function () {
      resolve(result)
      // }, 3000);
    }
  });
};



// ------------------------------------- GENERIC METHODS ---------------------------------------------



exports.completeMeterId = function completeMeterId(meterId, s) {

  const id = this.padWithChar(meterId, 12, s); //Add 0 to complete the 12 lenth address for the trama

  // Convert the string into array
  let pairs = [];
  _.map(id, function (l) { pairs.push(l); });

  // Make an array with pairs and make a reverse
  pairs = _.chunk(pairs, 2);
  pairs.reverse();

  //Join the string in order to have the address number to send in the trama
  pairs = pairs.join(',');
  pairs = pairs.replace(/\,/g, '');

  return pairs;
};


exports.padWithChar = function padWithChar(number, length, s) {
  return _.padLeft(number, length, s);
};


exports.formatIdentificationCode = function formatIdentificationCode(str) {

  const _this = this;
  let decimal = [];

  for (let i = 0; i < str.length; i += 2) {

    decimal[i] = _this.toDec(str[i]) + 3;
    decimal[i + 1] = _this.toDec(str[i + 1]) + 3;

    if (decimal[i + 1] > 9 && decimal[i + 1] <= 15) {
      decimal[i + 1] = hexArray[decimal[i + 1]];
    } else if (decimal[i + 1] > 15) {
      decimal[i] = decimal[i] + 1;
      decimal[i + 1] = hexArray[(decimal[i + 1] - 15) - 1];
    }

    if (decimal[i] > 9 && decimal[i] <= 15) {
      decimal[i] = hexArray[decimal[i]];
    } else if (decimal[i] > 15) {
      decimal[i] = hexArray[(decimal[i] - 15) - 1];
    }
  }

  str = decimal.join('');
  str = _this.reversePair(str);

  return str;
};

exports.toDec = function toDec(s) {
  return isNaN(parseInt(s, 16)) ? new String('Non-hex character entered') : parseInt(s, 16);
};

exports.elementDefaultPassFormatted = function getConductorKeyFormatted() {
  const convertPass = this.convertToHexa(elementsDefaultPass, true);
  return this.reversePair(convertPass);
};


exports.getFormatChunk = function getFormatChunk(str, start, end, flag) {
  const _this = this;
  let returnString = _.slice(str, start, end);

  returnString = returnString.join();
  returnString = returnString.replace(/\,/g, '');

  switch (flag) {

    case 'meterNumber':
      returnString = _this.reversePair(returnString);
      break;
    case 'data':
      returnString = _this.hexaToDec(returnString);
      //returnString = _this.reversePair(returnString);
      break;
    case 'identificationCode':
      returnString = _this.hexaToDec(returnString);
      returnString = _this.reversePair(returnString).toUpperCase();
      break;
    case 'binary':
      returnString = parseInt(returnString.toString(), 16).toString(2);
      returnString = returnString.length <= 1 ? '0000000000000000' : returnString;
      break;

  }

  return returnString;
};

exports.convertToHexa = function convertToHexa(str, reverse) {

  str = _.isString(str) ? str : str.toString();
  let resutl = '';

  for (let i = 0; i < str.length; i++) {
    let hexa = str[i];
    if (isNaN(hexa)) {
      const dec = str[i].toLowerCase();
      hexa = hexArray[dec];
    }

    hexa = parseInt(hexa) + 3;

    if (hexa > 9 && hexa <= 15) {
      hexa = hexArray[hexa];
    } else if (hexa > 15) {
      const index = hexa - 15;
      hexa = hexArray[index - 1];
    }

    resutl += hexa.toString();
  }


  if (reverse) {
    resutl = this.reversePair(resutl);
  }

  return resutl;
};

exports.hexaToDec = function hexaToDec(trama) {

  trama = trama.split('');
  let newTrama = '';

  for (let i = 0; i < trama.length; i++) {

    let v = trama[i][0].toUpperCase();
    v = parseInt(v, 16);

    v = v - 3;

    if (v > 9) {
      v = hexArray[v];
    } else if (v < 0) {
      let abs = Math.abs(v);
      abs = 10 + abs;
      v = hexArray[abs];
    }

    newTrama += v;

  }
  return newTrama;
};


exports.formatValidation = function formatValidation(idCodData, frameData) {
  const _this = this;

  if (idCodData.format && (idCodData.format.charAt(0) == 'X' || idCodData.format.charAt(0) == 'N')) { // format number
    return _this.formatPayloadToNumber(frameData, idCodData.format);
  } else if (idCodData.format && (idCodData.format.charAt(0))) { //format date or hour
    return _this.formatPayloadToDate(frameData, idCodData.format);
  }
};

exports.formatPayloadToNumber = function formatPayloadToNumber(data, format) {
  const separator = '.';
  const formatArray = format.split(separator);
  const position = data.length - formatArray.pop().length;

  data = [data.slice(0, position), separator, data.slice(position)].join('');

  return data = Number(data).toString();
};

exports.formatPayloadToDate = function formatPayloadToDate(data, format, cb) {
  data = moment(data, format);
  return data = data.format(format);
};


exports.getFormattedData = function getFormattedData(data, settings, reverse, element) {
  const self = this;
  const resutl = {};
  const originalData = _.clone(data);

  return new Promise(resolve => {
    data = data.toString();

    async.eachSeries(settings, (setting, cb) => {
      const splitSettings = setting.format.split('-');
      let currentData;
      currentData = _.clone(data.slice(0, splitSettings[0]));
      currentData = reverse ? self.reversePair(currentData) : currentData;

      data = data.slice(splitSettings[0], originalData.length);

      switch (splitSettings[1]) {

        case 'D':
          currentData = _.chunk(currentData, 6);

          let date = _.chunk(currentData[0], 2);
          let curr_date = new Date();
          date = date.join('-');
          date = date.replace(/,/g, '');
          date = String(curr_date.getFullYear()).substring(0, 2) + date;

          let time = _.chunk(currentData[1], 2);
          time = time.join(':');
          time = time.replace(/,/g, '');
          time = time + ':00';

          resutl[setting.fieldName] = date + ' ' + time;

          break;
        case 'N':

          let fixIndex = splitSettings[2] !== '0' ? (currentData.length - splitSettings[2]) - 1 : -1;
          let newString = '';

          for (let i = 0; i < currentData.length; i++) {

            if (i == fixIndex) {
              newString += currentData[i] + '.';
            } else {
              newString += currentData[i];
            }
          }


          resutl[setting.fieldName] = newString;

          break;
        default:
          resutl[setting.fieldName] = currentData;

      }
      cb();

    }, (err) => {
      resolve(resutl);
    });
  });
};


//---------------------------------------- CHEKSUM -----------------------------------------



exports.checksum8 = function checksum8(N) {

  // convert input value to upper case
  let strN = new String(N);
  let strResult;
  let strHex = new String('0123456789ABCDEF');

  strN = strN.toUpperCase();
  result = 0;
  fctr = 16;

  for (let i = 0; i < strN.length; i++) {
    if (strN.charAt(i) == ' ') continue;

    v = strHex.indexOf(strN.charAt(i));
    if (v < 0) {
      result = -1;
      break;
    }
    result += v * fctr;

    if (fctr == 16) fctr = 1;
    else fctr = 16;
  }

  if (result < 0) {
    strResult = new String('Non-hex character entered');
  }
  else if (fctr == 1) {
    strResult = new String('Odd number of characters entered. e.g. correct value = aa aa');
  }
  else {
    // Calculate 2's complement
    result = (~(result & 0xff) + 1) & 0xFF;
    // Convert result to string
    //strResult = new String(result.toString());
    strResult = strHex.charAt(Math.floor(result / 16)) + strHex.charAt(result % 16);
  }
  return strResult;
};

exports.checksum8ToModulo256 = function checksum8ToModulo256(N) {
  const dec = parseInt(N, 16);
  return isNaN(dec) ? new String('Non-hex character entered') : this.toHex((256 - dec));
};

exports.checksum256 = function checksum256(dataList, cb) {

  const checkSum8 = this.checksum8(dataList);
  const checkSumM256 = this.checksum8ToModulo256(checkSum8);

  if (cb) {
    return cb(null, checkSumM256);
  }

  return checkSumM256;

};


exports.reversePair = function reversePair(str) {
  let returnString = _.chunk(str, 2);
  returnString.reverse();
  returnString = returnString.join();
  returnString = returnString.replace(/\,/g, '');
  return returnString;
};


exports.toHex = function toHex(d) {
  return isNaN(Number(d)) ? new String('Non-hex character entered') : ('0' + (Number(d).toString(16))).slice(-2).toUpperCase();
};

exports.hexToBase64 = function hexToBase64(hexStr) {
  let base64 = ''
  for (let i = 0; i < hexStr.length; i++) {
    base64 += !(i - 1 & 1) ? String.fromCharCode(parseInt(hexStr.substring(i - 1, i + 1), 16)) : ''
  }
  return btoa(base64)
}