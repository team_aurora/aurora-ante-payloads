# ante-payloads Package #


## Objective ##

**ante-payloads package** provides utilities for encode and decode ante payloads.

## Installation ##

### Usage ###

* Install the package
```
#!javascript
npm install https://bitbucket.org/team_aurora/aurora-ante-payloads.git --save
```
* Define the config object
* Require it in your code
```
#!javascript

//config for ante-payloads library
const config = {
  trama: {
    firstHead : "68",
    seconHead : "68",
    tail: "16"
  },
  elementsDefaultPass: '0011011011'
};
var anteUtil = require('ante-payloads');
anteUtil.config(config); //define global vars in library
```

### Unit Test ###

* Run the test.sh
```
#!javascript

sh test.sh
```