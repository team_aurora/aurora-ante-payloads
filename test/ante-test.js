//config for ante-payloads library
const config = {
  trama: {
    firstHead : "68",
    seconHead : "68",
    tail: "16"
  },
  elementsDefaultPass: '0011011011'
};
const ante = require('./../index');
ante.config(config);
const chai = require('chai');
const assert = chai.assert;

describe('Checksums', () => {

  describe('#checksum8(), receive a Hex value (range = 0123456789ABCDEF)', () => {

    it('Check if returned string length is equal to 2', () => {
      const checksum8 = ante.checksum8('15 CB FF');
      assert.lengthOf(checksum8, 2);
    });

    it('Should return "Non-hex character entered" if the value entered not is Hex value', () => {
      const checksum8 = ante.checksum8('GG');
      assert.equal(checksum8, 'Non-hex character entered');
    });

    it('Should return "Odd number of characters entered. e.g. correct value = aa aa" if the entered value length is Odd.', () => {
      const checksum8 = ante.checksum8('AAA');
      assert.equal(checksum8, 'Odd number of characters entered. e.g. correct value = aa aa');
    });
  });
 
  describe('#checksum8toModulo256()', () => {
    it('Check if value returned its OK if entry is Hex value', () => {
      const checksum8To256 = ante.checksum8ToModulo256('77');
      const checksum8 = ante.checksum8('77');
      const okResponse = '89'; //precalculated

      assert.equal(checksum8To256, checksum8);
      assert.equal(checksum8To256, okResponse);
    });

    it('Check if value returned is equal to "Non-hex character entered" if the entry value is not a Hex value', () => {
      const checksum8To256 = ante.checksum8ToModulo256('GG');
      const response = 'Non-hex character entered'; //precalculated
      
      assert.equal(checksum8To256, response);
    });
  });

  describe('#checksum256()', () => {
  
    it('Check if value returned its OK if entry is Hex value.', () => {
      const checksum256 = ante.checksum256('AB 12 7A B1');
      const okResponse = 'E8'; //precalculated
      
      assert.lengthOf(checksum256, 2);
      assert.equal(checksum256, okResponse);
    });

    it('Check if value returned is equal to "Non-hex character entered" if the entry value is not a Hex value', () => {
      const checksum256 = ante.checksum256('thisisatest');
      const checksum256WithSpaces = ante.checksum256('this is a test');
      const response = 'Non-hex character entered'; //precalculated
      assert.equal(checksum256, response);
      assert.equal(checksum256WithSpaces, response);
    });
  });

  describe('#reversePair()', () => {
    it('Check if the output format is correct (input= "hello"  output= "loelh")', () => {
      const reverse = ante.reversePair('hello');
      const okResponse = 'ollhe'; //precalculated
      assert.equal(reverse, okResponse);
    });
  });

  describe('#toHex()', () => {
    it('Check if return value is tha last 2 digits from Hexadecimal representation of input number', () => {
      const hex = ante.toHex('1234567');
      const okResponse = ('12D687').slice(-2); //precalculated
      assert.equal(hex, okResponse);
      assert.lengthOf(hex, 2);
    });

    it('Should return "Non-hex character entered" if the input value is not a decimal number (or string number representation)', () => {
      const hex = ante.toHex('ABC');
      const response = 'Non-hex character entered'; //precalculated
      assert.equal(hex, response);
    });
  });
});


describe('Generic Methods', () => {
  describe('#padWithChar()', () => {
    it('Check if function return a correct length and value', () => {
      const value = ante.padWithChar('test', 15, '.');
      const okResponse = '...........test';
      assert.lengthOf(value, 15);
      assert.equal(value, okResponse);
    });
  });

  describe('#completeMeterId()', () => {
    it('Check if the returned value has correct format with length <= 12 (input= "hello", "0"  ==>  output= "loel0h000000")', () => {
      const id = ante.completeMeterId('hello', '0');
      const okResponse = 'loel0h000000'; //precalculated

      assert.lengthOf(id, 12);
      assert.equal(id, okResponse);
    });

    it('Check if the returned has correct format and the same input length with input length > 12', () => {
      const id = ante.completeMeterId('thisisaverygoodtest', '0');
      const okResponse = 'tesdtooygeravisisth'; //precalculated

      assert.lengthOf(id, 'thisisaverygoodtest'.length);
      assert.equal(id, okResponse);
    });
  });

  describe('#toDec()', () => {
    it('Check if returned value is a number if the entered value is a hexadecimal', () => {
      const dec = ante.toDec('ABC');
      assert.isNumber(dec);
    });

    it('Should return "Non-hex character entered" if the entered value is not hex', () => {
      const dec = ante.toDec('GGG');
      assert.equal(dec, 'Non-hex character entered');
    });
  });

  describe('#formatIdentificationCode()', () => {
    it('Check if the code returned is a not empty string if the entered value is a string', () => {
      const code = ante.formatIdentificationCode('this is a test');
      assert.isString(code);
      assert.isNotEmpty(code);
    });

    it('Check if returned value is empty string if the entered value is a number', () => {
      const code = ante.formatIdentificationCode(123);
      assert.isString(code);
      assert.isEmpty(code);
    });

    it('Check if returned value is empty string if the entered value is a empty string', () => {
      const code = ante.formatIdentificationCode('');
      assert.isString(code);
      assert.isEmpty(code);
    });
  });

  describe('#convertToHexa()', () => {
    it('Should return hexa string if the entered value is a decimal number (or string representation of number)', () => {
      const hexa = ante.convertToHexa('123');
      assert.isString(hexa);
      assert.isNotEmpty(hexa);
    });

    it('Should return hexa string reversePair if the entered value is a decimal number (or string representation of number)', () => {
      const hexa = ante.convertToHexa('123');
      const hexaReverse = ante.convertToHexa('123', true);
      const ok = '456'; //precalculated
      const okReverse = '645'; //precalculated

      assert.isString(hexaReverse);
      assert.isNotEmpty(hexaReverse);
      assert.equal(hexaReverse, ante.reversePair(hexa));
      assert.equal(hexaReverse, okReverse);
    });

    it('Check if the value return its incorrect if the value entered is not decimal', () => {
      const hexa = ante.convertToHexa('AB');
      assert.isString(hexa);
      assert.include(hexa, 'NaN');
    });
  });

  describe('#elementDefaultPassFormatted()', () => {
    it('Check if the returned value es a string and is not empty', () => {
      const format = ante.elementDefaultPassFormatted();
      assert.isString(format);
      assert.isNotEmpty(format);
    });
  });

  describe('#hexaToDec()', () => {
    it('Check if returned value is a string not empty if the entered value is a hex', () => {
      const dec = ante.hexaToDec('AA');
      assert.isString(dec);
      assert.isNotEmpty(dec);
    });
  });

  describe('#getFormatChunk()', () => {
    it('Should return string format value, for "meterNumber" flag', () => {
      const format = ante.getFormatChunk('thisisatest', 1, 7, 'meterNumber');
      assert.isString(format);
      assert.isNotEmpty(format);
    });

    it('Should return a string representacion of decimal if the entere value is a hex, for "data" flag', () => {
      const format = ante.getFormatChunk('AC1B4', 1, 7, 'data');
      assert.isString(format);
      assert.isNotEmpty(format)
    });

    it('Should return a string representation reversedPair of decimal if the entered value is a hex, for "identificacionCode" flag', () => {
      const format = ante.getFormatChunk('AC1B4', 1, 7, 'identificationCode');
      assert.isString(format);
      assert.isNotEmpty(format)
    });

    it('Should return a string representation binary if the entered value is a hex, for "binary" flag', () => {
      const format = ante.getFormatChunk('AC1B4', 1, 7, 'binary');
      assert.isString(format);
      assert.isNotEmpty(format)
    });

    it('Should return a "NaN" string if the entered value is NOT a hex, for "binary" flag', () => {
      const format = ante.getFormatChunk('thisisatest', 1, 7, 'binary');
      assert.isString(format);
      assert.isNotEmpty(format);
      assert.equal(format, 'NaN');
    });

    it('Should return a string representation sliced if the entered value is a hex, for other flag', () => {
      const format = ante.getFormatChunk('AC1B4', 1, 7, 'other case');
      assert.isString(format);
      assert.isNotEmpty(format)
      assert.equal(format, ('AC1B4').slice(1, 7));
    });
  });

  describe('#formatPayloadToNumber()', () => {
    it('Check if returned value has the correct format and type', () => {
      const format = 'ABC.DEFGHI';
      const number = ante.formatPayloadToNumber('123456789', format);
      assert.isString(number);
      assert.isNotEmpty(number);
      assert.isNotNaN(Number(number));
      assert.equal(number.indexOf('.'), format.indexOf('.'));
      assert.equal(number.length, format.length);
    });

    it('Should return NaN if the data entered value is not a number', () => {
      const number = ante.formatPayloadToNumber('abcdefghi', 'ABC.DEFGHI');
      assert.isTrue(isNaN(number));
    });
  });

  describe('#formatPayloadToDate()', () => {
    it('Check if the returned value has the correct format', () => {
      const time = new Date();
      const format = 'YYYYMMDD';
      const date = ante.formatPayloadToDate(time, format);

      assert.isString(date);
      assert.lengthOf(date, format.length);
      assert.isNotNaN(Number(date));
    });

    it('Check if the returned value is "Invalid date" if the entered value is not a date', () => {
      const time = 'I´m a date :)';
      const format = 'YYYYMMDD';
      const date = ante.formatPayloadToDate(time, format);
      const errorResponse = 'Invalid date';

      assert.isString(date);
      assert.equal(date, errorResponse);
    });
  });

  describe('#formatValidation()', () => {
    it('Check if returned value has the correct format if the entered value format is for number (XXXX.XXXX or NNN.NNNNN)', () => {
      const idCode = {
        format: 'NNNN.NNNN'
      };
      const frameData = '12345678';
      const response = ante.formatValidation(idCode, frameData);
      assert.isString(response);
      assert.isNotEmpty(response);
      assert.isNotNaN(Number(response));
      assert.equal(response.indexOf('.'), idCode.format.indexOf('.'));
      assert.equal(response.length, idCode.format.length);
    });

    it('Check if returned value has the correct format if the entered value format is for date', () => {
      const idCode = {
        format: 'YYYYMMDD'
      };
      const frameData = new Date();
      const date = ante.formatValidation(idCode, frameData);
      assert.isString(date);
      assert.lengthOf(date, idCode.format.length);
      assert.isNotNaN(Number(date));
    });
  });

  describe('#getFormattedData()', () => {
    it('Check if format is correct', async () => {
      settings = [
        {
          format: '3-D',
          fieldName: 'field1'
        },
        {
          format: '2-N',
          fieldName: 'field2'
        }
      ]
      const format = await ante.getFormattedData(12345, settings, true);
      assert.property(format, 'field1');
      assert.property(format, 'field2');
      assert.isNotNull(format.field1);
      assert.isNotNull(format.field2);
    });
  });
});

describe('Create write frames for services', () => {
   describe('#createWriteEnergyFrame()', () => {

     it('Check if returned value is a correct hexadecimal code formatted', async () => {
       const element = {
         elementId: 'BBC12F',
         schemaData: 'schema'
       }
       const identificationCode = 'AAB14F65A12C';
       const commandValue = '1234';
       const energyFrame = await ante.createWriteEnergyFrame(element, identificationCode, commandValue);
       assert.isString(energyFrame);
       assert.isNumber(parseInt(energyFrame, 16));
     });

     it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
       const element = {
         elementId: 'BBC12F',
         schemaData: 'schema'
       }
       const identificationCode = 'AAB';
       const commandValue = '12345';
       const energyFrame = await ante.createWriteEnergyFrame(element, identificationCode, commandValue);
       assert.isString(energyFrame);
       assert.isTrue(energyFrame.indexOf(' ') !== -1);
     });
   });


   describe('#createWriteWaterFrame()', () => {
     it('Check if returned value is a correct hexadecimal code formatted', async () => {
       const element = {
         elementId: 'BBC12F',
         schemaData: 'schema'
       };

       const idCodes = [
         {
           code: 'AAB14F65A12C',
           extra: {
            controlCode: 'ABCD',
            fieldLenght: 12,
            userDefined: 'ABC123'
           }
         }
       ];
       const identificationCode = 'AAB14F65A12C';
       const commandValue = '1234';
       const waterFrame = await ante.createWriteWaterFrame(element, identificationCode, idCodes, commandValue);
       
       assert.isString(waterFrame);
       assert.isNumber(parseInt(waterFrame, 16));
     });

     it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
       const element = {
         elementId: 'BBC12F',
         schemaData: 'schema'
       };

       const idCodes = [
        {
          code: 'AAB14F65A12C',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
       const identificationCode = 'AAB14F65A12C';
       const commandValue = '12345';
       const waterFrame = await ante.createWriteWaterFrame(element, identificationCode, idCodes, commandValue);
       assert.isString(waterFrame);
       assert.isTrue(waterFrame.indexOf(' ') !== -1);
     });
   });

   describe('#createWriteGasFrame()', () => {
     it('Check if returned value is a correct hexadecimal code formatted', async () => {
       const element = {
         elementId: 'BBC12F',
         schemaData: 'schema'
       };

       const idCodes = [
         {
           code: 'AAB14F65A12C',
           extra: {
            controlCode: 'ABCD',
            fieldLenght: 12,
            userDefined: 'ABC123'
           }
         }
       ];
       const identificationCode = 'AAB14F65A12C';
       const commandValue = '1234';
       const gasFrame = await ante.createWriteGasFrame(element, identificationCode, idCodes, commandValue);
       
       assert.isString(gasFrame);
       assert.isNumber(parseInt(gasFrame, 16));
     });

     it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
       const element = {
         elementId: 'BBC12F',
         schemaData: 'schema'
       };

       const idCodes = [
        {
          code: 'CCF12ACBBAD9',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
       const identificationCode = 'CCF12ACBBAD9';
       const commandValue = '12345';
       const gasFrame = await ante.createWriteGasFrame(element, identificationCode, idCodes, commandValue);
       assert.isString(gasFrame);
       assert.isTrue(gasFrame.indexOf(' ') !== -1);
     });
   });   
});

describe('Create read frame for services', () => {

  describe('#createEnergyReadFrame()', () => {
    it('Check if returned value is a correct hexadecimal code formatted', async () => {
      const element = {
        elementId: 'BBC12F',
        schemaData: 'schema'
      };
      const identificationCode = 'CCF12ACBBAD9';
      const energy = await ante.createEnergyReadFrame(element, identificationCode);
      assert.isString(energy);
      assert.isNumber(parseInt(energy, 16));
    });

    it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
      const element = {
        elementId: 'BBC12F',
        schemaData: 'schema'
      };
      const identificationCode = 'C';
      const energy = await ante.createEnergyReadFrame(element, identificationCode);
      assert.isString(energy);
      assert.isTrue(energy.indexOf(' ') !== -1);
    });
  });


  describe('#createEnergyReadRegisterFrame()', () => {
    it('Check if returned value is a correct hexadecimal code formatted', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const identificationCode = 'CAAACF12ACBBAD91CD';
      const payload = 'AAABBBCCCDDD1234567890';
      const energy = await ante.createEnergyReadRegisterFrame(element, identificationCode, payload);
      assert.isString(energy);
      assert.isNumber(parseInt(energy, 16));
      assert.isNotEmpty(energy);
    });

    it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const identificationCode = 'CAAACF12ACBBAD91C';
      const payload = 'AAABBBCCCDDD1234567890';
      const energy = await ante.createEnergyReadRegisterFrame(element, identificationCode, payload);
      assert.isString(energy);
      assert.isTrue(energy.indexOf(' ') !== -1);
    });
  });
  describe('#createWaterReadFrame()', () => {
    it('Check if returned value is a correct hexadecimal code formatted', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const idCodes = [
        {
          code: 'CAAACF12ACBBAD91CD',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
      const identificationCode = 'CAAACF12ACBBAD91CD';
      const energy = await ante.createWaterReadFrame(element, identificationCode, idCodes);
      assert.isString(energy);
      assert.isNumber(parseInt(energy, 16));
      assert.isNotEmpty(energy);
    });

    it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const idCodes = [
        {
          code: 'CAAACF12ACBBAD91C',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
      const identificationCode = 'CAAACF12ACBBAD91C';
      const energy = await ante.createWaterReadFrame(element, identificationCode, idCodes);
      assert.isString(energy);
      assert.isTrue(energy.indexOf(' ') !== -1);
    });
  });

  describe('#createGasReadFrame()', () => {
    it('Check if returned value is a correct hexadecimal code formatted', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const idCodes = [
        {
          code: 'CAAACF12ACBBAD91CD',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
      const identificationCode = 'CAAACF12ACBBAD91CD';
      const gasFrame = await ante.createGasReadFrame(element, identificationCode, idCodes);
      assert.isString(gasFrame);
      assert.isNumber(parseInt(gasFrame, 16));
      assert.isNotEmpty(gasFrame);
    });

    it('Check if returned value is a incorrect formatted hexadecimal if some entered value is not hex', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const idCodes = [
        {
          code: 'CAAACF12ACBBAD91C',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
      const identificationCode = 'CAAACF12ACBBAD91C';
      const gasFrame = await ante.createGasReadFrame(element, identificationCode, idCodes);
      assert.isString(gasFrame);
      assert.isTrue(gasFrame.indexOf(' ') !== -1);
    });
  });
});


describe('create read profile frame for energy', () => {

  describe('#createProfileFrame()', () => {
    it('Check if returned value is a correct hexadecimal code formatted', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const identificationCode = 'CAAACF12ACBBAD91C1';
      const loadLog = '12';
      const profile = await ante.createProfileFrame(element, identificationCode, loadLog);
      assert.isString(profile);
      assert.isNumber(parseInt(profile, 16));
      assert.isNotEmpty(profile);
    });

    it('Check if returned value is a incorrect formatted hexadecimal if some entered value identificationCode is not hex', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const identificationCode = 'CAA'; //odd
      const loadLog = '12';
      const profile = await ante.createProfileFrame(element, identificationCode, loadLog);
      assert.isString(profile);
      assert.isTrue(profile.indexOf(' ') !== -1);
    });
    it('Check if returned value is a incorrect formatted hexadecimal if some entered value loadLog  is not number dec', async () => {
      const element = {
        elementId: 'AAAAAA',
        schemaData: 'otherdate'
      };
      const identificationCode = 'CAA'; //odd
      const loadLog = '1A';
      const profile = await ante.createProfileFrame(element, identificationCode, loadLog);
      assert.isString(profile);
      assert.isTrue(profile.indexOf(' ') !== -1);
    });
  });
});

describe('Decoder energy', () => {

  describe('#energyDecoder()', () => {
    it('Check if the object returned has the correct format and any property is empty', async () => {
      //create the energy frame
      const element = {
        elementId: 'BBC12F',
        schemaData: 'schema'
      };
      const identificationCode = 'CCF12ACBBAD9';
      const energy = await ante.createEnergyReadFrame(element, identificationCode);
      //create the energy frame END

      const keys = [
        'firstHead',
        'meterNumber',
        'secondHead',
        'controlCode',
        'dataLength',
        'identificationCode',
        'data',
        'calibrationCode',
        'tail',
      ];
      const decodeEnergy = await ante.energyDecoder(energy);
      assert.isObject(decodeEnergy);
      assert.containsAllKeys(decodeEnergy, keys);
      assert.isTrue(Object.keys(decodeEnergy)
        .every(val => 
          decodeEnergy[val] !== ''
          && decodeEnergy[val].indexOf('NaN') === -1
          && decodeEnergy[val].indexOf(' ') === -1));
    });

    it('Check if any property value of returned object its incorrect (empty, NaN, backspace)', async () => {
      //create the energy frame
      const element = {
        elementId: 'BBC12F',
        schemaData: 'schema'
      };
      const identificationCode = 'CCF12ACBBAD';
      const energy = await ante.createEnergyReadFrame(element, identificationCode);
      //create the energy frame END

      const keys = [
        'firstHead',
        'meterNumber',
        'secondHead',
        'controlCode',
        'dataLength',
        'identificationCode',
        'data',
        'calibrationCode',
        'tail',
      ];
      const decodeEnergy = await ante.energyDecoder(energy);
      assert.isObject(decodeEnergy);
      assert.containsAllKeys(decodeEnergy, keys);
      assert.isTrue(Object.keys(decodeEnergy)
        .some(val => 
          decodeEnergy[val] !== ''
          && decodeEnergy[val].indexOf('NaN') === -1
          && decodeEnergy[val].indexOf('Non') === -1
          && decodeEnergy[val].indexOf('hex') === -1
          && decodeEnergy[val].indexOf(' ') === -1));
    });
  });


  describe('#waterDecoder()', () => {
    it('Check if the object returned has the correct format and any property is empty', async () => {
      //create the energy frame
      const element = {
        elementId: 'BBC12F',
        schemaData: 'schema'
      };
      const idCodes = [
        {
          code: 'CAAACF12ACBBAD91C1',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
      const identificationCode = 'CAAACF12ACBBAD91C1';
      const energy = await ante.createWaterReadFrame(element, identificationCode, idCodes);
      //create the energy frame END

      const keys = [
        'firstHead',
        'meterNumber',
        'secondHead',
        'controlCode',
        'dataLength',
        'identificationCode',
        'data',
        'calibrationCode',
        'tail',
      ];
      const decodeEnergy = await ante.waterDecoder(energy);
      assert.isObject(decodeEnergy);
      assert.containsAllKeys(decodeEnergy, keys);
      assert.isTrue(Object.keys(decodeEnergy)
        .every(val => 
          decodeEnergy[val] !== ''
          && decodeEnergy[val].indexOf('NaN') === -1
          && decodeEnergy[val].indexOf('Non') === -1
          && decodeEnergy[val].indexOf('hex') === -1
          && decodeEnergy[val].indexOf(' ') === -1));
    });

    it('Check if any property value of returned object its incorrect (empty, NaN, backspace)', async () => {
      //create the energy frame
      const element = {
        elementId: 'BBC12F',
        schemaData: 'schema'
      };
      const idCodes = [
        {
          code: 'CAAACF12ACBBAD91C',
          extra: {
           controlCode: 'ABCD',
           fieldLenght: 12,
           userDefined: 'ABC123'
          }
        }
      ];
      const identificationCode = 'CAAACF12ACBBAD91C';
      const energy = await ante.createWaterReadFrame(element, identificationCode, idCodes);
      //create the energy frame END

      const keys = [
        'firstHead',
        'meterNumber',
        'secondHead',
        'controlCode',
        'dataLength',
        'identificationCode',
        'data',
        'calibrationCode',
        'tail',
      ];
      const decodeEnergy = await ante.waterDecoder(energy);
      assert.isObject(decodeEnergy);
      assert.containsAllKeys(decodeEnergy, keys);
      assert.isTrue(Object.keys(decodeEnergy)
        .some(val => 
          decodeEnergy[val] !== ''
          && decodeEnergy[val].indexOf('NaN') === -1
          && decodeEnergy[val].indexOf(' ') === -1));
    });
  });
  
});